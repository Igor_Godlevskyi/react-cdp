import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './components/App';
import TasksPage from './components/task/TasksPage';
import ManageTaskPage from './components/task/ManageTaskPage'; //eslint-disable-line import/no-named-as-default
import CategoryPage from './components/category/CategoryPage';
export default (
  <Route path="/" component={App}>
    <IndexRoute component={CategoryPage}/>
    <Route path="category-:id" component={CategoryPage}>
      <IndexRoute component={TasksPage}/>          
      <Route path="editTask-:idTask" component={ManageTaskPage} />
    </Route>   
  </Route>
);
