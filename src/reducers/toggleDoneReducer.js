import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function toggleState(state = initialState.done, action) {
    switch (action.type) {
        case types.TOGGLE_TASK_STATE:
            return !state;
        default:
            return state || false;
    }
}