import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function taskFilter(state = initialState.filterQuery, action) {
    switch (action.type) {
        case types.FILTER_TASKS:
            return action.data;
        default:
            return state || '';
    }
}