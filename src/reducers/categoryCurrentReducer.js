import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function editTask(state = initialState.category, action) {
    switch (action.type) {
        case types.SET_CURRENT_CATEGORY:
            return action.category;
        default:
            return state;
    }
}