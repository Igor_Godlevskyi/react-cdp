import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function taskReducer(state = initialState.tasks, action) {
  switch (action.type) {
    
    case types.LOAD_TASKS_SUCCESS:
      return action.tasks;

    case types.CREATE_TASK_SUCCESS:
      return  [Object.assign({}, action.task)].concat(state);      

    case types.UPDATE_TASK_SUCCESS:
      let taskUpdate = action.task;
      return state.map((task) => (task.id === taskUpdate.id) ?
        Object.assign({}, task, taskUpdate) : task
      );

    default:
      return state;
  }
}
