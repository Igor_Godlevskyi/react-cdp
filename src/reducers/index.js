import {combineReducers} from 'redux';
import tasks from './taskReducer';
import task from './taskCurrentReducer';
import categories from './categoryReducer';
import category from './categoryCurrentReducer';
import done from './toggleDoneReducer';
import filterQuery from './tasksFilterReducer';
import ajaxCallsInProgress from './ajaxStatusReducer';

const rootReducer = combineReducers({
  tasks,
  task,
  categories,
  category,
  done,
  filterQuery,
  ajaxCallsInProgress
});

export default rootReducer;
