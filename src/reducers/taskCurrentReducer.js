import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function editTask(state = initialState.task, action) {
    switch (action.type) {
        case types.SET_CURRENT_TASK:
            return action.task || null;
        default:
            return state || null;
    }
}