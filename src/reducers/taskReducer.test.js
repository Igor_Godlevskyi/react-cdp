import expect from 'expect';
import taskReducer from './taskReducer';
import * as actions from '../actions/taskActions';

describe('task Reducer', () => {
  it('should add task when passed CREATE_TASK_SUCCESS', () => {
    // arrange
    const initialState = [
      {id:'A',title: 'A'},
      {id:'B',title: 'B'}
    ];

    const newTask = {id:'C', title: 'C'};

    const action = actions.createTaskSuccess(newTask);

    //act
    const newState = taskReducer(initialState, action);

    //assert
    expect(newState.length).toEqual(3);
    expect(newState[0].title).toEqual('C');
    expect(newState[1].title).toEqual('A');
    expect(newState[2].title).toEqual('B');
  });

  it('should update task when passed UPDATE_TASK_SUCCESS', () => {
    // arrange
    const initialState = [
      {id: 'A', title: 'A'},
      {id: 'B', title: 'B'},
      {id: 'C', title: 'C'}
    ];

    const task = {id: 'B', title: 'New Title'};
    const action = actions.updateTaskSuccess(task);

    // act
    const newState = taskReducer(initialState, action);
    const updatedTask = newState.find(a => a.id == task.id);
    const untouchedTask = newState.find(a => a.id == 'A');

    // assert
    expect(updatedTask.title).toEqual('New Title');
    expect(untouchedTask.title).toEqual('A');
    expect(newState.length).toEqual(3);
  });
});
