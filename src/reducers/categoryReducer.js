import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function categoryReducer(state = initialState.categories, action) {
  switch (action.type) {    
    case types.LOAD_CATEGORIES_SUCCESS:
      return action.categories;              
    case types.ADD_SUB_CATEGORY_SUCCESS:      
      function addSubCategory(categories) {            
            
            return categories.map(cat => {
            
                if (cat.id === action.data.parentId) {
                    cat.childNodes = cat.childNodes || [];
                    cat.childNodes.unshift({id: action.data.category.id, categoryName: action.data.category.categoryName, visible: false, childNodes: []});
                }
                else if (cat.childNodes) {
                    addSubCategory(cat.childNodes);
                }
                return cat;
            });
        }
        // Kind of a deep copy of the state              
        return addSubCategory(JSON.parse(JSON.stringify(state))) ;
        
    case types.CREATE_CATEGORY_SUCCESS:
      return [Object.assign({}, action.category)].concat(state);        
  
    case types.DELETE_CATEGORY_SUCCESS:
        // Kind of a deep copy of the state    
        let categories =JSON.parse(JSON.stringify(state));
        function deleteCategory(state) {
            state.forEach(function (cat, index) {
                if (cat.id === action.id) {
                    state.splice(index, 1);
                }
                else if (cat.childNodes) {
                    deleteCategory(cat.childNodes);
                }
            });
        }
        deleteCategory(categories);
        return categories;
    case types.UPDATE_CATEGORY_SUCCESS:
      // Kind of a deep copy of the state   
      let categoriesCopy = JSON.parse(JSON.stringify(state));
      function updateCategory(state) {
            state.forEach(function (cat, index) {
                if (cat.id === action.data.id) {
                    cat.categoryName  = action.data.newName;
                }
                else if (cat.childNodes) {
                    updateCategory(cat.childNodes);
                }
            });
        }
       updateCategory(categoriesCopy);
       return categoriesCopy;     
   
    default:
      return state;
  }
}
