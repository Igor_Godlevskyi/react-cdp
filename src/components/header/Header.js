import React, {PropTypes} from 'react';
import HeaderTools from './HeaderTools';
import ProgressBar from '../progressBar/ProgressBar';
import {Link} from 'react-router';
import {connect} from 'react-redux';


class Header extends React.Component {
    constructor(props) {
    super(props);

  }
    render() {
        return (
            <div className="Header">
                <div className="toolbar-wrapper form-inline">
                    <h2> {(Object.keys(this.props.task).length !== 0) ? this.props.task.title : 'To-Do List'} </h2>
                    {(Object.keys(this.props.task).length === 0) && <HeaderTools/>}
                </div>
                <ProgressBar/>
            </div>
        );
    }

}

Header.propTypes = {
    task: PropTypes.object
};

const mapStateToProps = ({task}) => ({
    task
});

export default connect(mapStateToProps)(Header);