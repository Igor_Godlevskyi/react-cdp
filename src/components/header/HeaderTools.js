import React, {PropTypes} from 'react';
import ReactDOM from 'react-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as filterActions from '../../actions/filterActions';


class HeaderTools extends React.Component {
    
    constructor(props){
        super(props);
        this.formQueryString = this.formQueryString.bind(this);        
    }
   
    componentWillMount() {         
        if (this.props.done !== (this.context.router.location.query.done ? JSON.parse(this.context.router.location.query.done): this.props.done)) {            
            this.props.actions.toggleDone(this.props.done); 
        }
        if (this.props.filterQuery !== (this.context.router.location.query.searchFilter ? this.context.router.location.query.searchFilter: this.props.filterQuery)) {            
            this.props.actions.filterTask(this.context.router.location.query.searchFilter); 
        }
    }
    encodeQueryData(data) {
        let ret = [];
        for (let d in data){
            ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));            
        }
        return ret.join('&');
    }    
    formQueryString(e){
        let queryParams = this.context.router.location.query;
        let queryString = "";
        const field = e.target.id;
        if (field ==="doneFilter"){
             this.props.actions.toggleDone(this.props.done); 
             e.target.checked?  queryParams.done = true : delete queryParams.done;  
        }
        if (field ==="searchFilter"){
             this.props.actions.filterTask(e.target.value);
             e.target.value !=="" ? queryParams.searchFilter = e.target.value : delete queryParams.searchFilter;
        }
        if (field ==="clearSearchFilter"){
             this.props.actions.filterTask("");
             delete queryParams.searchFilter;            
        }
        
        queryString = this.encodeQueryData(queryParams);        
        this.pushQueryToUrl(queryString);       

    }
    pushQueryToUrl(queryString){
        this.context.router.push(this.context.router.location.pathname + (queryString ? "?" + queryString: ""));
    }
    render() {
        let props = this.props;
       
        return (<div className="toolbar">
            <div className="toggle-tasks form-group">                
                <label className="checkbox-inline">
                    <input type="checkbox"  id="doneFilter"
                        checked={this.props.done ? "checked" :  ""}
                        onChange={this.formQueryString}
                        className="form-control"/>ShowDone
                </label>                        
            </div>
            <div className="search-tasks form-group">
                <input className="form-control" id="searchFilter"
                       type="search"
                       value={this.props.filterQuery}
                       onChange={this.formQueryString}
                       ref="searchInput"
                       placeholder="Search"/>               
                <i onClick={this.formQueryString} id="clearSearchFilter" className="material-icons clear">clear</i>
            </div>
        </div>);
    }

    
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(filterActions, dispatch)
  };
}
function mapStateToProps(state, ownProps) {   
    return {
        done: state.done,
        filterQuery:state.filterQuery
    };
}
HeaderTools.propTypes = {    
    actions: PropTypes.object.isRequired,
    done: PropTypes.bool.isRequired,
    filterQuery: PropTypes.string.isRequired
};
//Pull in the React Router context so router is available on this.context.router.
HeaderTools.contextTypes = {
  router: PropTypes.object.isRequired
};


export default connect(mapStateToProps, mapDispatchToProps)(HeaderTools);
