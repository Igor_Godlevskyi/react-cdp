import React, {PropTypes} from 'react';
import ReactDOM from 'react-dom';

class Form extends React.Component {
    constructor(props){
        super(props);
        this.addItem = this.addItem.bind(this);
    }
    
    addItem(event) {
        event.preventDefault();
        let input = ReactDOM.findDOMNode(this.refs.input);
        if (input.value) {
            this.props.addItem(input.value);
            input.value = '';
        }
    }
    render() {
        return (
            <div className="InputForm">
                <form className="form-inline" action="" onSubmit={this.addItem}>
                    <div className="form-group">
                        <input className="form-control" type="text" ref="input" placeholder={this.props.placeholder}/>
                        
                        <button type="submit"
                                className="btn btn-primary">
                            Add
                        </button>
                    </div>
                </form>
            </div>
        );
    }    
}
Form.propTypes = {
  placeholder: PropTypes.string.isRequired,
  addItem: PropTypes.func.isRequired
};
export default Form;