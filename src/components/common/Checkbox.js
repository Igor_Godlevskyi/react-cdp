import React, {PropTypes} from 'react';

const Checkbox = ({name, label, onChange, value}) => {

  return (
      <span>
        <input
          type="checkbox"
          name={name}
          checked={value}
          onChange={onChange}/>      
     
        <label htmlFor={name}>{label}</label>
      </span>

  );
};

Checkbox.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  value: PropTypes.string
};

export default Checkbox;
