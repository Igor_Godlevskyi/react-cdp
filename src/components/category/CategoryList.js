import React, {PropTypes} from 'react';
import CategoryListRow from './CategoryListRow';

const CategoryList = ({categories,                       
                       task,
                       addSubCategory,
                       deleteCategory,
                       updateCategory,
                       done,
                       filterQuery,
                       updateTaskCategory}, context) => {
  return (      
      <ul className="list-group">
      {categories.map(function(category){
        return (
          <CategoryListRow deleteCategory={deleteCategory} 
                           addSubCategory={addSubCategory}                          
                           updateCategory ={updateCategory}
                           key={category.id} 
                           category={category}
                           done={done}
                           task={task}
                           filterQuery={filterQuery}
                           updateTaskCategory={updateTaskCategory}
                           searchString = {context.router.location.search}/>);
    })} 
      </ul>);
};

CategoryList.propTypes = {
  categories: PropTypes.array.isRequired, 
  updateTaskCategory: PropTypes.func.isRequired,
  task: PropTypes.object.isRequired,
  addSubCategory: PropTypes.func.isRequired,
  deleteCategory: PropTypes.func.isRequired,
  updateCategory: PropTypes.func.isRequired,
  done: PropTypes.bool.isRequired,
  filterQuery:  PropTypes.string.isRequired
};
//Pull in the React Router context so router is available on this.context.router.
 CategoryList.contextTypes = {
  router: PropTypes.object.isRequired
};
export default CategoryList;
