import React, {PropTypes} from 'react';
import SkyLightConfirm from 'react-skylight';

class ConfirmModal extends React.Component {
  constructor(props){
    super(props);
    
  }  
  show(){
      this.refs.confirmModal.show();
  }
  render() {
         
    const styles = {
        confirmDialogStyles : {
            width: '50%',
            height: '180px',
            position: 'fixed',
            top: '50%',
            left: '50%',
            marginTop: '-200px',
            marginLeft: '-25%',
            backgroundColor: '#fff',
            borderRadius: '2px',
            zIndex: 100,
            padding: '15px',
            boxShadow: '0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28)'
        },
        contentWrapper :{
            display: "flex",
            justifyContent: "center"
        },
        button: {
            width: "100px",
            marginRight: "20px",
            marginLeft: "20px"
        },
        title:{
            textAlign: "center"    
        }
  };

    return (
        <SkyLightConfirm 
            title ={this.props.title}
            dialogStyles={styles.confirmDialogStyles}
            hideOnOverlayClicked
            titleStyle = {styles.title}
            ref="confirmModal" >
                <div style={styles.contentWrapper}>                 
                    <button style= {styles.button} className="btn btn-primary" onClick={() =>  this.props.onOk()}>{this.props.okText || "Ok"}</button>
                    <button style= {styles.button} className="btn btn-primary" onClick={() => this.refs.confirmModal.hide()}>{this.props.cancelText || "Cancel"}</button>  
                </div>
        </SkyLightConfirm>
    );
  }
}

ConfirmModal.propTypes = {
  title: PropTypes.string.isRequired,
  okText:PropTypes.string.isRequired,
  cancelText: PropTypes.string.isRequired,
  onOk:PropTypes.func.isRequired  
};

export default ConfirmModal;