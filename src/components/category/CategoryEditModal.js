import React, {PropTypes} from 'react';
import SkyLight from 'react-skylight';

class CategoryEdit extends React.Component {
  constructor(props){
    super(props);
    this.save = this.save.bind(this);
}
  show(){
      this.refs.editModal.show();
  }
  close(){
      this.refs.editModal.hide(); 
  }
  save(){
      this.props.onSave(this.props.category.id,this.refs.name.value);
      this.refs.editModal.hide(); 
  }
  render() {
    const styles = {
        confirmDialogStyles: {
            width: '50%',
            height: '250px',
            position: 'fixed',
            top: '50%',
            left: '50%',
            marginTop: '-200px',
            marginLeft: '-25%',
            backgroundColor: '#fff',
            borderRadius: '2px',
            zIndex: 100,
            padding: '15px',
            boxShadow: '0 0 4px rgba(0,0,0,.14),0 4px 8px rgba(0,0,0,.28)'
        },
        modalWrapper:{
            color: 'grey'
        }
    };

    return (
        <SkyLight dialogStyles={styles.confirmDialogStyles}  hideOnOverlayClicked ref="editModal" title= {this.props.title}>
            <div className="form" style={styles.modalWrapper}>
                <div className="form-group">
                    <input className="form-control" autoFocus type="text" ref="name" placeholder={this.props.placeholder} defaultValue={this.props.initialValue}/>
                </div>
                <div className="form-group">
                    <button className="btn btn-primary form-control" onClick={this.save}>Save</button>
                </div>                
            </div> 
        </SkyLight>
    );
  }
}

CategoryEdit.propTypes = {
  category: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  initialValue:PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  onSave: PropTypes.func.isRequired
};

export default CategoryEdit;