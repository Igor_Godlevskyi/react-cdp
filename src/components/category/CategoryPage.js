import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as categoryActions from '../../actions/categoryActions';
import * as taskActions from '../../actions/taskActions';
import CategoryList from './CategoryList';
import InputForm from '../common/InputForm';
import {browserHistory} from 'react-router';
import toastr from 'toastr';

class CategoryPage extends React.Component {
  constructor(props) {
    super(props);
    this.addCategory = this.addCategory.bind(this);
    this.addSubCategory = this.addSubCategory.bind(this); 
    this.deleteCategory = this.deleteCategory.bind(this);
    this.updateCategory = this.updateCategory.bind(this);
    this.selectCategory = this.selectCategory.bind(this);
    this.updateTaskCategory = this.updateTaskCategory.bind(this);       
  }
  
  componentWillReceiveProps(nextProps) {   
    // This is done to set the active category state when category ID is recieved from the URL
    // e.g. http://localhost:3000/category-sub-category-1.1
    if ((Object.keys(nextProps.category).length !== 0) && nextProps.category.id !== this.props.category.id){
      this.selectCategory(nextProps.category);
    }     
  }

  addCategory(categoryName) {        
    this.props.actions.addCategory({
      id: null,
      categoryName: categoryName,
      visible: false,
      childNodes:[]
    })
    .then(() => {
        toastr.success(`Category: ${categoryName} added`);       
      })
      .catch(error => {
        toastr.error(error);        
      });
  }
  addSubCategory(parentId,name){
    this.props.actions.addSubCategory(parentId,name)
    .then(() => {
        toastr.success(`Sub-Category ${name} added`);        
      })
      .catch(error => {
        toastr.error(error);        
      });
    
  }
  deleteCategory(categoryId){
    this.props.actions.deleteCategory(categoryId)
    .then(() => {
        toastr.success(`Category deleted`);        
        this.props.actions.selectCategory({});
        this.props.router.push(`/${this.props.router.location.search}`);
      })
      .catch(error => {
        toastr.error(error);        
      });
  }
  updateCategory(id,name){
   this.props.actions.updateCategorySuccess(id,name);     
  }
  selectCategory(category){
    this.props.actions.selectCategory(category);
  }
  updateTaskCategory(currentTask, newCategory){
     let movedTask = Object.assign({},currentTask);
     movedTask.categoryId =  newCategory.id;
     this.props.taskActions.updateTask(movedTask)
      .then(() => {
        toastr.success(`Task ${ movedTask.title} category changed to ${newCategory.categoryName}`);
        
        this.props.taskActions.setCurrentTask(movedTask);
        this.props.router.push(`/category-${newCategory.id}/editTask-${movedTask.id}${this.props.router.location.search}`);
      })
      .catch(error => {
        toastr.error(error);        
      });
    
  }
  render() {
    const {categories,done,filterQuery,task} = this.props;
    return (
       <div className="row">
          <div className="col-sm-4">
             <InputForm addItem={this.addCategory} placeholder="Add new category"/>
            <CategoryList                 
              categories={categories}             
              task={task}
              updateCategory={this.updateCategory}
              deleteCategory={this.deleteCategory}
              addSubCategory={this.addSubCategory}
              selectCategory={this.selectCategory}
              done={done}
              filterQuery={filterQuery}
              updateTaskCategory={this.updateTaskCategory}
              />
          </div>
          <div className="col-sm-8">            
            {(Object.keys(this.props.category).length !== 0) ? this.props.children : 
              <div className="nocategory"> <h2>Please, select category!</h2> </div>}
          </div>
      </div>  
      
    );
  }
}

CategoryPage.propTypes = {
  categories: PropTypes.array.isRequired,
  category: PropTypes.object.isRequired,
  task: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  taskActions: PropTypes.object.isRequired,
  children: PropTypes.object,
  done: PropTypes.bool,
  filterQuery: PropTypes.string,
  router: PropTypes.object.isRequired 
};
function getCategoryById(categories, id) {
  let category = {};
  function rec (categories, id) {   
    categories.forEach(function (cat) {
      if (cat.id === id) {
        category = Object.assign({},cat);
      } else if (cat.childNodes) {
      rec(cat.childNodes,id);
      }    
    });
  }
  rec(categories, id);
  return category;
}
function mapStateToProps(state, ownProps) {
  const idCategory = ownProps.params.id;
  let category = {};
  if (idCategory && state.categories.length > 0) {
    category = getCategoryById(state.categories, idCategory); 
  } 
  return {
    category: category,
    categories: state.categories,
    done: state.done,
    filterQuery: state.filterQuery,
    task: state.task
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(categoryActions, dispatch),
    taskActions: bindActionCreators(taskActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoryPage);
