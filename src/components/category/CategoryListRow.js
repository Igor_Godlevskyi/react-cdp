import React, {PropTypes} from 'react';
import {Link, withRouter} from 'react-router';
import CategoryEditModal from './CategoryEditModal';
import ConfirmDeleteModal from './ConfirmDeleteModal';

export default class CategoryListRow extends React.Component { 
  constructor(props) {
    super(props);
    this.state = {
      visible: true
    };    
    this.toggleVisible = this.toggleVisible.bind(this);       
  }
   toggleVisible(){
    this.setState({visible: !this.state.visible});
  }
    
  render(){
    const {category, filterQuery, done, searchString} = this.props;    
    let childNodes;
    let self = this;
    if (category.childNodes && category.childNodes.length !== undefined && category.childNodes.length > 0)  {
      childNodes = category.childNodes.map(function(category, index){
                return (
                 <CategoryListRow                   
                    deleteCategory ={self.props.deleteCategory}                   
                    key={category.id} 
                    category={category}
                    searchString={searchString}
                    task={self.props.task}
                    updateTaskCategory={self.props.updateTaskCategory}                    
                    updateCategory={self.props.updateCategory}
                    addSubCategory={self.props.addSubCategory}/>
                );
            });
    }
    return (
    <li key={category.id} className="list-group-item">
      {(childNodes && childNodes.length > 0) && 
        (this.state.visible ? <i onClick ={this.toggleVisible} className="material-icons tree">keyboard_arrow_down</i> :
                              <i onClick ={this.toggleVisible} className="material-icons tree">keyboard_arrow_right</i>)}     
      <Link 
        to={`/category-${category.id}${searchString}`} 
        activeClassName="active">        
          {category.categoryName}
      </Link>

      {(Object.keys(this.props.task).length == 0) ?
        <span>
          <i onClick ={() => this.refs.editCategoryModal.show()} className="material-icons categoryEdit">edit</i>
          <i onClick ={() => this.refs.deleteConfirm.show()} className="material-icons delete">delete</i>
          <i onClick ={() => this.refs.addCategoryModal.show()} className="material-icons add">add</i> 
        </span> :
        <i onClick ={() => this.props.updateTaskCategory(this.props.task,category)} className="material-icons move">reply</i>
      }
      
      <CategoryEditModal onSave={this.props.updateCategory}
                         hideOnOverlayClicked
                         ref="editCategoryModal"
                         category={category}
                         title={`Edit category: ${category.categoryName}`}
                         initialValue={category.categoryName}
                         placeholder="Category name"/>
      <CategoryEditModal onSave={this.props.addSubCategory}
                         hideOnOverlayClicked
                         ref="addCategoryModal"
                         category={category}
                         title={`Add sub-category to the ${category.categoryName}`}
                         initialValue=""
                         placeholder="Sub-category name"/>
      <ConfirmDeleteModal ref = "deleteConfirm"
                          title= {`Delete Category ${category.categoryName}?`}
                          okText="Yes" 
                          cancelText="No"
                          onOk={this.props.deleteCategory.bind(this,category.id)}/>
      
      {(childNodes && childNodes.length > 0) &&
        <ul className = {`list-group inner ${this.state.visible ? "childOpeneded" : "childClosed"}`} >
          {childNodes}
        </ul>}
      
    </li>
    
    );
  }
}


 CategoryListRow.propTypes = {
  task: PropTypes.object.isRequired, 
  category: PropTypes.object.isRequired,
  addSubCategory: PropTypes.func.isRequired,
  updateCategory: PropTypes.func.isRequired,
  updateTaskCategory: PropTypes.func.isRequired,
  deleteCategory: PropTypes.func.isRequired,
  done: PropTypes.bool.isRequired,
  filterQuery:  PropTypes.string.isRequired,
  searchString: PropTypes.string.isRequired  
};

