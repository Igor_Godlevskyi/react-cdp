import React from 'react';
import TextInput from '../common/TextInput';
import TextArea from '../common/TextArea';
import Checkbox from '../common/Checkbox';

const TaskForm = ({task, onSave, onChange, saving, errors, onCancel}) => {
  return (
    <form>     
      <span className="taskEditButtons">
        <input
          type="submit"
          disabled={saving}
          value={saving ? "Saving changes..." : "Save changes"}
          className="btn btn-primary"
          onClick={onSave}/>        
        <input
          type="submit"
          disabled={saving}
          value="Cancel"
          className="btn btn-primary taskTitleInput"
          onClick={onCancel}/>
       </span> 

      <TextInput
        name="title"
        label=""
        placeholder="Title"
        value={task.title}
        onChange={onChange}
        error={errors.title}/>

      <Checkbox
        name="isDone"
        label="Done"
        value={task.isDone? "checked" : ""}
        onChange={onChange}      
      />
      <TextArea
        name="description"
        label=""                
        placeholder = "Description"
        value={task.description}
        onChange={onChange}
        error={errors.description}/>

      
    </form>
  );
};

TaskForm.propTypes = {
  task: React.PropTypes.object.isRequired,  
  onSave: React.PropTypes.func.isRequired,
  onChange: React.PropTypes.func.isRequired,
  saving: React.PropTypes.bool,
  errors: React.PropTypes.object,
  onCancel: React.PropTypes.func
};

export default TaskForm;
