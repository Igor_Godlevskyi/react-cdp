import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as taskActions from '../../actions/taskActions';
import TaskList from './TaskList';
import {browserHistory} from 'react-router';
import InputForm from '../common/InputForm';
import toastr from 'toastr';


class TasksPage extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.setTask = this.setTask.bind(this);
    this.addTask = this.addTask.bind(this);
    this.saveTask = this.saveTask.bind(this); 
  }
  setTask(task) {    
    //this.props.actions.setCurrentTask(task);
    browserHistory.push(`/category-${task.categoryId}/editTask-${task.id}`);
 
 }
  getTaskByIdforSpecifiedCategory(id){
    return this.props.tasks.filter((task)=> {
      (task.id === id) && (task.categoryId === this.props.params.id);  
    });
  } 
  saveTask(task) {
    this.setState({saving: true});
    this.props.actions.updateTask(task)     
      .then(()=> toastr.success(`Task ${task.title} saved`))
      .catch(error => {
        toastr.error(error);
        this.setState({saving: false});
      });
  }
  addTask(taskName) {        
    this.props.actions.addTask({
      id: null,
      categoryId: this.props.params.id,
      title: taskName,    
      isDone: false,   
      description: ''
    })
     .then(()=> toastr.success(`Task ${taskName} added`))
      .catch(error => {
        toastr.error(error);
        this.setState({saving: false});
      });
  }

  render() {   
    const matchFilter = (filter, task) => (!filter || task.title.match(filter));
    const matchDone = (state, task) => (!state || task.isDone);
    const {tasks, done, filterQuery} = this.props;    
    
    return (        
        <div className="taskInputForm">          
          <InputForm   addItem={this.addTask} placeholder="Add new task"/>          
          <TaskList                     
            tasks={tasks.filter((task)=> {
               return (task.categoryId === this.props.params.id &&
                      matchFilter(this.props.filterQuery,task) &&
                      matchDone(this.props.done, task));
              }
            )}
            setTask = {this.setTask}
            updateTaskdone={this.saveTask}/>
        </div>    
    );
  }
}

TasksPage.propTypes = {
  tasks: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired,
  done: PropTypes.bool,
  filterQuery: PropTypes.string,
  params: PropTypes.object
};

function mapStateToProps(state, ownProps) {
  return {
    tasks: state.tasks,
    done: state.done,
    filterQuery:state.filterQuery
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(taskActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TasksPage);
