import React from 'react';
import expect from 'expect';
import {mount, shallow} from 'enzyme';
import {ManageTaskPage} from './ManageTaskPage';

describe ('Manage Task Page', () => {
  it('sets error message when trying to save empty title', () => {
    const props = {
      actions: { saveTask: () => { return Promise.resolve(); }},
      task: {id: '', title: '', categoryId: '', isDone: false, description: ''}
    };
    const wrapper = mount(<ManageTaskPage {...props}/>);
    const saveButton = wrapper.find('input').first();
    expect(saveButton.prop('type')).toBe('submit');
    saveButton.simulate('click');
    expect(wrapper.state().errors.title).toBe('Title must be at least 5 character.');

  });
});
