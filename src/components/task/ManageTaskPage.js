import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {browserHistory} from 'react-router';
import {bindActionCreators} from 'redux';
import * as taskActions from '../../actions/taskActions';
import TaskForm from './TaskForm';
import toastr from 'toastr';
 
export class ManageTaskPage extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      task: Object.assign({}, props.task),
      errors: {},
      saving: false
    };
    this.updateTaskState = this.updateTaskState.bind(this);
    this.saveTask = this.saveTask.bind(this);
    this.canselEditing = this.canselEditing.bind(this);
  }

  componentWillMount(){
    this.props.actions.setCurrentTask(this.props.task);
  }
  
  componentWillReceiveProps(nextProps) {        
      this.setState({task: Object.assign({}, nextProps.task)});
      if (nextProps.task.id && nextProps.task.id !== this.state.task.id ){
        this.props.actions.setCurrentTask(nextProps.task);
      }      
  }  
  componentWillUpdate(nextProps, nextState) {    

    if (nextProps.task.id && nextProps.task.id !== this.state.task.id ){
        this.props.actions.setCurrentTask(nextProps.task);
    }
  }

  componentWillUnmount(){
    this.props.actions.setCurrentTask({});
  }
  updateTaskState(event) {   
    this.taskFormIsValid();
    const field = event.target.name;
    let task = this.state.task;
    if(field === "isDone"){
      task[field] = event.target.checked;
    }else{
      task[field] = event.target.value;    
    }
    return this.setState({task: task});
  }
  
  taskFormIsValid() {
 
    let formIsValid = true;
    let error = {};
    if (this.state.task.title.length <= 5) {
      error.title = "Title must be at least 5 character.";
      formIsValid = false;
    }
    this.setState({errors: error});
    return formIsValid;
  }
  saveTask(event) {
    event.preventDefault();
    if (!this.taskFormIsValid()) {
      return;
    }
    this.setState({saving: true});
    this.props.actions.updateTask(this.state.task)
      .then(() => this.redirect())
      .catch(error => {
        toastr.error(error);
        this.setState({saving: false});
      });
  }
  canselEditing(event){
    event.preventDefault();
    this.setState({saving: false});
    toastr.success('Saving canceled');
    this.props.router.push(`/category-${this.props.task.categoryId}${this.props.router.location.search}`);
  }
  redirect() {
    this.setState({saving: false});
    toastr.success('Task saved');
    this.props.router.push(`/category-${this.props.task.categoryId}${this.props.router.location.search}`);
    
  }
  render() {
    return ( 
      <div>
        {(this.state.task.id) ?
        <TaskForm
          onChange={this.updateTaskState}
          onSave={this.saveTask}
          task={this.state.task}
          errors={this.state.errors}
          saving={this.state.saving}
          onCancel ={this.canselEditing}
        /> :
        <h2 className="noTask">
          No such task, please check the URL
        </h2>}
      </div>
    );
  }
}

ManageTaskPage.propTypes = {
  task: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  router: PropTypes.object.isRequired
};

function getTaskById(tasks, id) {
  let task = tasks.filter(task => task.id === id); 
  if (task.length) return task[0];  //since filter returns an array, have to grab the first.
  return {};
}

function mapStateToProps(state, ownProps) {
  let task = {};
  const idTask = ownProps.params.idTask;  

  if (idTask && state.tasks.length > 0) {
    task = getTaskById(state.tasks, idTask); 
  }  
  return {
    task: task
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(taskActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageTaskPage);
