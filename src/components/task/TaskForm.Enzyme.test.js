import expect from 'expect';
import React from 'react';
import {mount, shallow} from 'enzyme';
import TestUtils from 'react-addons-test-utils';
import TaskForm from './TaskForm';

function setup(saving) {
  const props = {
    task: {}, saving: saving, errors: {},
    onSave: () => {},
    onChange: () => {}
  };

  return shallow(<TaskForm {...props} />);
}

describe('TaskForm via Enzyme', () => {
  it('renders form', () => {
    const wrapper = setup(false);
    expect(wrapper.find('form').length).toBe(1);    
  });

  it('save button is labeled "Save changes" when not saving', () => {
    const wrapper = setup(false);
    expect(wrapper.find('input').first().props().value).toBe('Save changes');
  });

  it('save button is labeled "Saving changes..." when saving', () => {
    const wrapper = setup(true);
    expect(wrapper.find('input').first().props().value).toBe('Saving changes...');
  });
});
