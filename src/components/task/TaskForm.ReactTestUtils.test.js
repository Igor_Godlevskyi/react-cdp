import expect from 'expect';
import React from 'react';
import TestUtils from 'react-addons-test-utils';
import TaskForm from './TaskForm';

function setup(saving) {
  let props = {
    task: {}, saving: saving, errors: {},
    onSave: () => {},
    onChange: () => {}
  };

  let renderer = TestUtils.createRenderer();
  renderer.render(<TaskForm {...props}/>);
  let output = renderer.getRenderOutput();

  return {
    props,
    output,
    renderer
  };
}

describe('TaskForm via React Test Utils', () => {
  it('renders form', () => {
    const { output } = setup();
    expect(output.type).toBe('form');
  });
});
