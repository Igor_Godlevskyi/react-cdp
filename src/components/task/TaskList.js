import React, {PropTypes} from 'react';
import TaskListRow from './TaskListRow';

class TaskList extends React.Component {
  constructor(props) {
    super(props);
  
  }
  render(){
  let {tasks, setTask, updateTaskdone} = this.props;
  return (
    <div className = "list-group">      
      {tasks.map(task =>
        <TaskListRow setTask={setTask} key={task.id} task={task} updateTaskdone = {updateTaskdone}/>
      )}
    </div>
  );
  }
}

TaskList.propTypes = {
  tasks: PropTypes.array.isRequired,
  setTask: PropTypes.func.isRequired,
  updateTaskdone: PropTypes.func.isRequired
};

export default TaskList;
