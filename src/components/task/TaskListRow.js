import React, {PropTypes} from 'react';
import Checkbox from '../common/Checkbox';

class TaskListRow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      task: Object.assign({}, props.task)    
    };  
    this.updateTaskDone = this.updateTaskDone.bind(this); 
  }
  
  updateTaskDone(event) {    
    let task = this.state.task;
    task.isDone = event.target.checked;
    this.setState({task: task});
    this.props.updateTaskdone(task);  
  }
  render(){
    let {task, setTask} = this.props;
    return (
      <div className = "list-group-item">    
        <Checkbox value={this.state.task.isDone ? "checked" : ""} name="isDone" label="" onChange={this.updateTaskDone}/>
        {task.title}
        <i onClick = {() => setTask(task)} className="material-icons taskEdit">edit</i>    
      </div>
    );
}
}

TaskListRow.propTypes = {
  task: PropTypes.object.isRequired,
  setTask:PropTypes.func.isRequired,
  updateTaskdone:PropTypes.func.isRequired
};

export default TaskListRow;
