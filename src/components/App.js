// This component handles the App template used on every page.
import React, {PropTypes} from 'react';
import Header from './header/Header';
import {connect} from 'react-redux';
import CategoryPage from './category/CategoryPage';
class App extends React.Component {
  
  render() {
    return (
     
      <div className="container-fluid">
        <div className="row">
          <Header />
        </div>
        <div className="row">          
            {this.props.children}                 
        </div>        
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.object
};

function mapStateToProps(state, ownProps) {

  return {
    loading: state.ajaxCallsInProgress > 0
  };
}

export default connect(mapStateToProps)(App);
