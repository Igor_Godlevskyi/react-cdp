import React, {PropTypes} from 'react';
import {connect} from 'react-redux';



class ProgressBar extends React.Component {

    render() {
       let tasksAll = this.props.tasks.length;
       let categories = JSON.parse(JSON.stringify(this.props.categories));
       let tasks = JSON.parse(JSON.stringify(this.props.tasks));
       let tasksNew = [];
       let progress = 0;
      
       if (tasks.length > 0 && categories.length > 0){       
               
                function recAddTasks(categories){
                    categories.forEach(function(cat){     
                        let catTasks = tasks.filter(function(tas){
                            return cat.id === tas.categoryId;
                        });                    
                        catTasks.forEach((ta)=> tasksNew.push(ta) );                    
                        if (isAllTasksDone(catTasks || cat.childNodes.length === 0 )){
                        return;
                        }else {                       
                        recAddTasks(cat.childNodes); 
                        }
                
                    });
                } 
                function isAllTasksDone(catTasks){
                   return (catTasks.filter(function(t,i){
                        return (t.isDone === true);
                   }).length === catTasks.length ? true : false
                   );
                }
               
                recAddTasks(categories);
            
            let newTasksLength = tasksNew.length;            
            let tasksDone = tasksNew.filter((task) => { 
                return task.isDone;
            });
            let tasksDoneLength = tasksDone.length;
            
            progress = parseInt((100 * (tasksDoneLength / newTasksLength)).toFixed(), 10);
        }
        return (<div className="ProgressBar">
            <progress max="100" value={progress}/>
        </div>);
    }
}
const mapStateToProps = ({tasks,categories}) => ({
    tasks,
    categories
});
ProgressBar.propTypes = {
    tasks: PropTypes.array.isRequired,
    categories: PropTypes.array.isRequired
};


export default connect(mapStateToProps)(ProgressBar);

