import delay from './delay';

// This file mocks a web API by working with the hard-coded data below.
// It uses setTimeout to simulate the delay of an AJAX call.
// All calls return promises.


let categories = [
  {
    id: 'first-category',
    categoryName: 'Category 1',
    childNodes:[{
      id: 'sub-category-1.1',
      categoryName: 'Category 1.1',
      visible: false,
      childNodes:[{
        id: 'sub-category-1.1.1',
        categoryName: 'Category 1.1.1',
        visible: false,
        childNodes: []
      },
      {
        id: 'sub-category-1.1.2',
        categoryName: 'Category 1.1.2',
        visible: false,
        childNodes:[]
      },{
        id: 'sub-category-1.1.3',
        categoryName: 'Category-1.1.3',
        visible: false,
        childNodes:[]
      }]
    },
    {
      id: 'sub-category-1.2',
      categoryName: 'Category 1.2',
      visible: false,
      childNodes:[]
    }]
  },
  {
    id: 'second-category',
    categoryName: 'Category 2',
    visible: false,
    childNodes:[]
  },
  {
    id: 'third-category',
    categoryName: 'Category 3',
    visible: false,
    childNodes:[]
  }
];

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, 'g'), replace);
}
//This would be performed on the server in a real app.
const generateId = (category) => {
  let d = new Date();
  let timeStamp = d.getTime();
  return replaceAll(category.categoryName, ' ', '-') + timeStamp.toString();
};
function addSubCategoryRec(categories ,parentId, category) {

  return categories.map(cat => {
    if (cat.id === parentId) {
      cat.childNodes = cat.childNodes || [];
      cat.childNodes.unshift(category);
    }
    else if (cat.childNodes) {
      addSubCategoryRec(cat.childNodes);
    }
   return cat;
  });
}

class CategoryApi {
  static getAllCategories() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(Object.assign([], categories));
      }, delay);
    });
  }

  static saveCategory(category) {
    category = Object.assign({}, category);
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        // Simulateing server validation
        const minCategoryNameLength = 5;
        if (category.categoryName.length < minCategoryNameLength) {
          reject(`Category name must be at least ${minCategoryNameLength} characters.`);
        }
        const existingCategoryIndex = categories.findIndex(a => a.id == category.id);
        if (existingCategoryIndex === -1) {
          category.id = generateId(category);
          categories.unshift(category);
        } else {
          categories.splice(existingCategoryIndex, 1, category);
        }
        resolve(category);
      }, delay);
    });
  }
  
  static addSubCategory(parentid,name) {
     let categoryNew = {
      id: "",
      categoryName: name,
      visible: false,
      childNodes:[]
    };    
    categoryNew.id = generateId(categoryNew);   
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        // Simulate server-side validation
        const minCategoryNameLength = 5;
        if (categoryNew.categoryName.length < minCategoryNameLength) {
          reject(`Category name must be at least ${minCategoryNameLength} characters.`);
        }        
        addSubCategoryRec(JSON.parse(JSON.stringify(categories)),parentid,categoryNew);
        resolve(categoryNew);
      }, delay);
    });
  }

  static deleteCategory(categoryId) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const indexOfCategoryToDelete = categories.findIndex(category => {
          category.categoryId == categoryId;
        });
        categories.splice(indexOfCategoryToDelete, 1);
        resolve(categoryId);
      }, delay);
    });
  }
}

export default CategoryApi;
