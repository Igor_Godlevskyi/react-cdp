import delay from './delay';

// This file mocks a web API by working with the hard-coded data below.
// It uses setTimeout to simulate the delay of an AJAX call.
// All calls return promises.
let tasks = [
  {
    id: "first-task",
    title: "First task",
    categoryId: "first-category",
    isDone: true,   
    description: "Description for the task 1"
  },
  {
    id: "second-task",
    title: "Clean Code: Writing Code for Humans",   
    categoryId: "second-category",
    isDone: false,   
    description: "Description for the task 2"
  },
  {
    id: "third-task",
    title: "Architecting Applications for the Real World",
    categoryId: "second-category",
    isDone: true,
    description: "Description for the task 3"
  },
  {
    id: "fourth-task",
    title: "Becoming an Outlier: Reprogramming the Developer Mind",
    categoryId: "third-category",
    isDone: false,
    description: "Description for the task 4"
  },
  {
    id: "six-task",
    title: "Web Component Fundamentals",
    categoryId: "third-category",
    isDone: true,
    description: "Description for the task 5"
  }
];

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, 'g'), replace);
}

//This would be performed on the server in a real app. Just stubbing in.
const generateId = (task) => {
  let d = new Date();
  let timeStamp = d.getTime();
  return replaceAll(task.title, ' ', '-') + timeStamp.toString();
};

class TaskApi {
  static getAllTasks() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(Object.assign([], tasks));
      }, delay);
    });
  }

  static saveTask(task) {
    task = Object.assign({}, task); // to avoid manipulating object passed in.
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        // Simulate server-side validation
        const minTaskTitleLength = 5;
        if (task.title.length < minTaskTitleLength) {
          reject(`Task Name must be at least ${minTaskTitleLength} characters.`);
        }
        const existingTaskIndex = tasks.findIndex(a => a.id == task.id);
        if (existingTaskIndex === -1) {
          task.id = generateId(task);         
          tasks.unshift(task);         
        } else {          
          tasks.splice(existingTaskIndex, 1, task);
        }

        resolve(task);
      }, delay);
    });
  }

  static deleteTask(taskId) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const indexOfTaskToDelete = tasks.findIndex(task => {
          task.taskId == taskId;
        });
        tasks.splice(indexOfTaskToDelete, 1);
        resolve();
      }, delay);
    });
  }
}

export default TaskApi;
