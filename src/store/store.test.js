import expect from 'expect';
import { createStore } from 'redux';
import rootReducer from '../reducers';
import initialState from '../reducers/initialState';
import * as taskActions from '../actions/taskActions';

describe('Store', function() {
  it('Should handle creating tasks', function() {
    // arrange
    const store = createStore(rootReducer, initialState);
    const task = {
      title: "Clean Code"
    };

    // act
    const action = taskActions.createTaskSuccess(task);
    store.dispatch(action);

    // assert
    const actual = store.getState().tasks[0];
    const expected = {
      title: "Clean Code"
    };

    expect(actual).toEqual(expected);
  });
});
