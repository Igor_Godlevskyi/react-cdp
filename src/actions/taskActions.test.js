import expect from 'expect';
import * as taskActions from './taskActions';
import * as types from './actionTypes';

import thunk from 'redux-thunk';
import nock from 'nock';
import configureMockStore from 'redux-mock-store';

// Test a sync action
describe('Task Actions', () => {
  describe('createTaskSuccess', () => {
    it('should create a CREATE_TASK_SUCCESS action', () => {
      //arrange
      const task = {id: 'clean-code', title: 'Clean Code'};
      const expectedAction = {
        type: types.CREATE_TASK_SUCCESS,
        task: task
      };

      //act
      const action = taskActions.createTaskSuccess(task);

      //assert
      expect(action).toEqual(expectedAction);
    });
  });
});

const middleware = [thunk];
const mockStore = configureMockStore(middleware);

describe('Async Actions', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('should create BEGIN_AJAX_CALL and LOAD_TASKS_SUCCESS when loading tasks', (done) => {
       const expectedActions = [
      {type: types.BEGIN_AJAX_CALL},
      {type: types.LOAD_TASKS_SUCCESS, body: {tasks: [{id: 'first-task', title: 'Clean Code'}]}}
    ];

    const store = mockStore({tasks: []}, expectedActions, done);
    store.dispatch(taskActions.loadTasks()).then(() => {
      const actions = store.getActions();
      expect(actions[0].type).toEqual(types.BEGIN_AJAX_CALL);
      expect(actions[1].type).toEqual(types.LOAD_TASKS_SUCCESS);
      done();
    });
  });
});
