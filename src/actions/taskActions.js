import * as types from './actionTypes';
import taskApi from '../api/mockTaskApi';
import {beginAjaxCall, ajaxCallError} from './ajaxStatusActions';

export function loadTasksSuccess(tasks) {
  return { type: types.LOAD_TASKS_SUCCESS, tasks};
}

export function createTaskSuccess(task) {  
  return {type: types.CREATE_TASK_SUCCESS, task};
}

export function updateTaskSuccess(task) {
  return {type: types.UPDATE_TASK_SUCCESS, task};
}
export function setCurrentTask(task) {
  return {type:types.SET_CURRENT_TASK,task};
}

export function loadTasks() {
  return function(dispatch) {
    dispatch(beginAjaxCall());
    return taskApi.getAllTasks().then(tasks => {
      dispatch(loadTasksSuccess(tasks));
    }).catch(error => {
      throw(error);
    });
  };
}

export function addTask(task) {
  return function (dispatch, getState) {
    dispatch(beginAjaxCall());
    return taskApi.saveTask(task).then(task => {
       dispatch(createTaskSuccess(task));
    }).catch(error => {
      dispatch(ajaxCallError(error));
      throw(error);
    });
  };
}
export function updateTask(task) {
  return function (dispatch, getState) {
    dispatch(beginAjaxCall());
    return taskApi.saveTask(task).then(task => {
    dispatch(updateTaskSuccess(task)); 
    }).catch(error => {
      dispatch(ajaxCallError(error));
      throw(error);
    });
  };
}
