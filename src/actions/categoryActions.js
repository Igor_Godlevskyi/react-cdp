import CategoryApi from '../api/mockCategoryApi';
import * as types from './actionTypes';
import {beginAjaxCall, ajaxCallError} from './ajaxStatusActions';

export function loadCategoriesSuccess(categories) {
  return {type: types.LOAD_CATEGORIES_SUCCESS, categories};
}
export function createCategorySuccess(category) {
  return {type: types.CREATE_CATEGORY_SUCCESS, category};
}
export function updateCategorySuccess(id,name) { 
  return {type: types.UPDATE_CATEGORY_SUCCESS, 
          data: {
            id:id,
            newName:name
          }
        };
}
export function deleteCategorySuccess(id){
  return {type: types.DELETE_CATEGORY_SUCCESS, id};
}

export function addSubCategorySuccess(category,parentId){

  return {
    type: types.ADD_SUB_CATEGORY_SUCCESS,
    data:{
      category: category,
      parentId : parentId
    }
  };
}
export function loadCategories() {
  return dispatch => {
    dispatch(beginAjaxCall());
    return CategoryApi.getAllCategories().then(categories => {
      dispatch(loadCategoriesSuccess(categories));
    }).catch(error => {
      throw(error);
    });
  };
}
export function addCategory(category) {

  return function (dispatch, getState) {
    dispatch(beginAjaxCall());
    return CategoryApi.saveCategory(category).then(category => {
        dispatch(createCategorySuccess(category));        
    }).catch(error => {
      dispatch(ajaxCallError(error));
      throw(error);
    });
  };
}
export function deleteCategory(id) {

  return function (dispatch, getState) {
    dispatch(beginAjaxCall());
    return CategoryApi.deleteCategory(id).then(id => {
        dispatch(deleteCategorySuccess(id));
    }).catch(error => {
      dispatch(ajaxCallError(error));
      throw(error);
    });
  };
}
export function addSubCategory(parentId, name) {
  return function (dispatch, getState) {
    dispatch(beginAjaxCall());    
    return CategoryApi.addSubCategory(parentId, name).then((category) => {
        dispatch(addSubCategorySuccess(category, parentId));
    }).catch(error => {
      dispatch(ajaxCallError(error));
      throw(error);
    });
  };
}
/*
export function updateCategory(id,name) {
  return function (dispatch, getState) {
    dispatch(beginAjaxCall());
    return CategoryApi.saveCategory(category).then(category => {
        dispatch(updateCategorySuccess(category));
    }).catch(error => {
      dispatch(ajaxCallError(error));
      throw(error);
    });
  };
}

*/
export function selectCategory(category) {  
  return {type: types.SET_CURRENT_CATEGORY, category};
}

