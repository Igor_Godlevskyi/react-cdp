import * as types from './actionTypes';

export function toggleDone(state) {
  return { type: types.TOGGLE_TASK_STATE, data: state};
}

export function filterTask(query) {
  return { type: types.FILTER_TASKS, data: query};
}



